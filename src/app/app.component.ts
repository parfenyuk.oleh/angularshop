import { Component } from '@angular/core';
import { PetShop } from './models/PetShop';
import { Cat } from './models/Cat';
import { Dog } from './models/Dog';
import { Hamster } from './models/Hamster';
import { Pet } from './models//Pet';

@Component({
    selector: 'pet-shop',
    styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `],
    templateUrl: './app.components.html',
    styleUrls: ['./app.components.css']
})
export class PetShopComponent {
    types: string[] = ['Cat', 'Dog', 'Hamster'];
    listTitle: string;
    exeption: string;
    selectedPets: Pet[];
    petShop: PetShop;
    currentPet = 'Cat';
    constructor () {
        this.petShop = new PetShop();
        this.petShop.addPets([
            new Cat('white', 453, 'Jeka', true),
            new Cat('grey', 334, 'Bonya', true),
            new Cat('white', 234, 'Pepe', false),
            new Dog('black', 654, 'Muhtar'),
            new Dog('brown', 123, 'Tobik'),
            new Dog('white', 987, 'Lolo'),
            new Hamster('black', 176, true),
            new Hamster('yellow', 431, true),
            new Hamster('white', 830, false)
        ]);
    }
    validInput(color: string, price: number, name: string) {
        if ( (color || price || name) === 'undefined') {
            // tslint:disable-next-line:no-unused-expression
            this.exeption = 'efg';
         }
    }
    addPet(color: string, price: number, name: string, isflufy: boolean) {
        if (this.currentPet === 'Cat') {
            this.petShop.addPets([new Cat (color, price, name, isflufy)]);
        } else if (this.currentPet === 'Dog') {
            this.petShop.addPets([new Dog (color, price, name)]);
        } else if (this.currentPet === 'Hamster') {
            this.petShop.addPets([new Hamster (color, price, isflufy)]);
        }
     }
     changeForm(newPet: string): void {
        this.currentPet = newPet;
    }
    showPetList (value: string) {
        this.listTitle = value;

        if (value === 'Cats') {
            this.selectedPets = this.petShop.getByType(Cat);
        } else if (value === 'ExpensivePets') {
            this.selectedPets = this.petShop.getGreaterThanEvaragePrice();
        } else if (value === 'Fluffy') {
            this.selectedPets = this.petShop.getFluffyAndWhite();
        }
    }
}
